# EE 524 Project


## Running experiments

#### 1. Setting up

- Install requirements from `requirements.txt`
- Keep books in `./gutenberg` as mentioned in README inside.

#### 2. Train word2vec model.

```
cd ./src/utils
python train_word2vec.py
```

#### 3. Use notebook

`./src/main.ipynb`

Code uses parts written by Abhinav Dahiya nad Abhinav Tushar.

